# This file is part of sale_pos module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.modules.product import price_digits

__all__ = ['Template', 'Product']


def round_num(value):
    return Decimal(str(round(value, 4)))


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'
    extra_tax = fields.Numeric('Extra Tax', digits=(16, 2))
    last_cost = fields.Numeric('Last Cost', digits=price_digits,
        help='Last purchase price')
    sale_price_taxed = fields.Function(fields.Numeric('Sale Price Taxed',
        digits=(16, 4)), 'get_sale_price_taxed')
    cost_price_taxed = fields.Function(fields.Numeric('Cost Price Taxed',
        digits=(16, 4)), 'get_cost_price_taxed')

    def get_cost_price_taxed(self, name=None):
        res = self.last_cost or self.cost_price
        price = self.last_cost or self.cost_price
        taxes = self.template.account_category.supplier_taxes_used
        for tax in taxes:
            if tax.type == 'percentage' and tax.rate and tax.rate > 0:
                res += price * (tax.rate)
            elif tax.type == 'fixed':
                res += self.extra_tax or tax.amount or 0
        return round_num(res)

    def get_sale_price_taxed(self, name=None):
        res = self.list_price
        taxes = self.template.account_category.customer_taxes_used
        for tax in taxes:
            if tax.type == 'percentage' and tax.rate and tax.rate > 0:
                res += self.list_price * (tax.rate)
            elif tax.type == 'fixed':
                if self.extra_tax:
                    res += self.extra_tax
                else:
                    res += tax.amount
        return round_num(res)


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'
    sale_price_w_tax = fields.Numeric('Sale Price With Tax', digits=(16, 2),
        depends=['list_price', 'customer_taxes'], required=True)

    # @fields.depends('customer_taxes', 'sale_price_w_tax', 'customer_taxes_used')
    # def on_change_with_list_price(self):
    #     if self.sale_price_w_tax:
    #         res = self.compute_reverse_list_price(self.sale_price_w_tax)
    #         return res

    def compute_reverse_list_price(self, price_w_tax):
        Tax = Pool().get('account.tax')
        res = Tax.reverse_compute(price_w_tax, self.customer_taxes_used)
        res = res.quantize(
            Decimal(1) / 10 ** self.__class__.list_price.digits[1])
        return res
