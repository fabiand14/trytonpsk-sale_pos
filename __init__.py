# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.pool import Pool
import sale
import shop
import statement
import product
import device
import user
import invoice
import tax


def register():
    Pool.register(
        tax.Tax,
        invoice.Invoice,
        product.Template,
        device.SaleDevice,
        user.User,
        statement.Journal,
        sale.Sale,
        sale.SaleLine,
        statement.Statement,
        statement.StatementLine,
        product.Product,
        sale.SalePaymentForm,
        shop.SaleShop,
        shop.ShopDailySummaryStart,
        sale.SaleUpdateDateStart,
        sale.SaleDetailedStart,
        sale.SaleIncomeDailyStart,
        sale.SaleByKindStart,
        sale.PortfolioPaymentsStart,
        device.SaleDeviceStatementJournal,
        statement.OpenStatementStart,
        statement.OpenStatementDone,
        statement.CloseStatementStart,
        statement.CloseStatementDone,
        statement.BillMoney,
        statement.MoneyCount,
        statement.ExpensesDaily,
        module='sale_pos', type_='model')
    Pool.register(
        shop.ShopDailySummaryReport,
        sale.SaleReportSummary,
        sale.SaleReportSummaryByParty,
        sale.SaleDetailedReport,
        sale.SaleIncomeDailyReport,
        sale.SaleByKindReport,
        sale.PortfolioPaymentsReport,
        module='sale_pos', type_='report')
    Pool.register(
        sale.SaleDetailed,
        sale.SaleForceDraft,
        sale.SaleUpdateDate,
        shop.ShopDailySummary,
        sale.WizardSalePayment,
        sale.SaleIncomeDaily,
        sale.SaleByKind,
        sale.DeleteSalesDraft,
        sale.PortfolioPayments,
        statement.OpenStatement,
        statement.CloseStatement,
        module='sale_pos', type_='wizard')
