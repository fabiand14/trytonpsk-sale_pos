# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.transaction import Transaction

__all__ = ['Tax']


class Tax(metaclass=PoolMeta):
    __name__ = 'account.tax'

    # def _process_tax(self, price_unit):
    #     res = super(Tax, self)._process_tax(price_unit)
    #     if self.type == 'fixed':
    #         product = Transaction().context.get('product')
    #         if product and product.extra_tax:
    #             res['amount'] = product.extra_tax
    #     return res
